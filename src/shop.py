class Order:
    def __init__(self, customer_email):
        self.customer_email = customer_email
        self.products = []
        self.purchased = False

    def add_product(self, product):

        self.products.append(product)

    def get_total_price(self):

        price = []

        for n in self.products:
            price.append(n.get_price())

        return sum(price)

    def get_total_quantity_of_products(self):

        quantities = 0

        for n in self.products:
            quantities += n.quantity

        return quantities

    def purchase(self):

        self.purchased = True


class Product:

    def __init__(self, name, unit_price, quantity=1):
        self.name = name
        self.unit_price = unit_price
        self.quantity = quantity

    def get_price(self):
        return self.unit_price * self.quantity


if __name__ == '__main__':
    test_order = Order('adrian@example.com')
    print(test_order.customer_email)

    shoes = Product('Shoes', 30.00, 3.0)
    test_order.add_product(shoes)
    print(test_order.get_total_price())

    test_order = Order('adrian@example.com')
    print(test_order.customer_email)
    print(test_order.get_total_price())

    shoes = Product('Shoes', 30.00, 3.0)
    tshirt = Product('T-shirt', 50.00, 2.0)
    bag = Product('Bag', 10.00)
    test_order.add_product(shoes)
    test_order.add_product(tshirt)
    test_order.add_product(bag)
    print(test_order.get_total_price())
    print(test_order.get_total_quantity_of_products())

    test_order.purchase()
    print(test_order.purchased)
